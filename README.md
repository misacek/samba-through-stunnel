![Build Status](https://gitlab.com/misacek/samba-through-stunnel/badges/master/build.svg)
![Overall test coverage](https://gitlab.com/misacek/samba-through-stunnel/badges/master/coverage.svg)
![Core Infrastructure Initiative Best Practices](https://bestpractices.coreinfrastructure.org/projects/1167/badge)


Check [gitlab pages](https://misacek.gitlab.io/samba-through-stunnel/) for documentation.

