
include $(shell pwd)/Makefile.common
include $(shell pwd)/Makefile.variables

MYSELF := TOP/Makefile

.DEFAULT_GOAL := all

all: doc
	@echo $(shell date) $(MYSELF): success: $@ target

help:
	@echo "Possible targets: build install clean test doc all:default"
	@echo
	@echo "You can specify the following ENV variables: tbd :)"

# target to allow running: make QAlib
$(SUBDIRS): $(BUILD_SUBDIRS)

build: $(BUILD_SUBDIRS)
	@echo $(shell date) $(MYSELF): success: $@ target
$(BUILD_SUBDIRS):
	$(MAKE) $(SUBMAKES_PARAMS) -C $(@:build-%=%) build
.PHONY: $(BUILD_SUBDIRS) build

install: $(INSTALL_SUBDIRS)
	@echo $(shell date) $(MYSELF): success: $@ target
$(INSTALL_SUBDIRS):
	$(MAKE) $(SUBMAKES_PARAMS) -C $(@:install-%=%) install
.PHONY: $(INSTALL_SUBDIRS) install

$(CLEAN_SUBDIRS): # run 'make clean' in subdirectories
	$(MAKE) $(SUBMAKES_PARAMS) -C $(@:clean-%=%) clean

REMOVE_DIRS := $(RPM_BUILD_ROOT) $(TITO_BUILD_ROOT) $(MOCK_BUILD_ROOT) 
clean: $(CLEAN_SUBDIRS) $(REMOVE_DIRS)
	$(RM) -r $(REMOVE_DIRS)
	@echo $(shell date) $(MYSELF): success: $@ target
.PHONY: clean

$(TEST_SUBDIRS): # run 'make test' in subdirectories
	$(MAKE) $(SUBMAKES_PARAMS) -C $(@:test-%=%) test
check: test
test: $(TEST_SUBDIRS)
	@echo $(shell date) $(MYSELF): success: $@ target
.PHONY: $(TEST_SUBDIRS) check test
