#!/bin/bash
#
# General behaviour:
# Look for shares on $1 (with $CREDFILE or $CREDDIR/$1 for authentication) and
# then look for whether there is separate authentication file for each share
# and if so, use it. This way we can have different authentication for each
# share.

#
# The behaviour of the script is as follows:
#   ./ $KEY is the hostname of smb host
#   ./ If there is $CREDFILE then it is used for authentication and the
#       $CREDDIR is ignored
#   ./ If there is no $CREDFILE defined and authentication is necessary for
#       browsing there must exist $CREDDIR/<hostname>[:<port>] file
#   ./ If you have more than one share and you want to have different
#       authentication for each of the shared you might create file
#       $CREDDIR/<hostname>[:port].<sharename> and it will be used for
#       that share authentication otherwise
#       $CREDDIR/<hostname>[:<port>] will be used

trap "echo 'Unexpected exit, run with DEBUG=1.'; exit 1;" ERR
[ -n "$DEBUG" ] && set -x
logger='logger -t autofs.smbng'

# This file must be executable to work! chmod 755!

# IC patch
#
# The problem with the original file was that it was executing smbclient -gL
# $key But $key contains the full path e.g.: server/share/subdir This was
# causing auto.smb to go to an endless loop This modified file corrects this
# problem by passing just the host to smbclient and in addition it provides a
# simple method to locate the credentials file
# The changed parts are marked as "IC patch"
#
# Please read the creddir variable description to see how to provide
# credentials files.
#
# When you search for an inexistent share, or you authentication fails the
# script behaves the wrong way This is because smbclient does protest, it just
# falls back like calling it as smbclient -gL host A check at the smbclient
# output should be done to avoid this, but I haven't figured it out yet
# Ioannis Chronakis <i.chronakis at gmail dot com>
#

export KEY="$1"
$logger "Called with argument: $KEY"

# find group that we will use
GROUP=root
for g in wheel sudo pirate; 
do
    if grep -q ^$g /etc/group;
    then
        GROUP=$g
        break
    fi
done
MOUNTOPTS="fstype=cifs,rw,uid=${UID},gid=$GROUP"
smbopts=()

# IC patch
# Look for credentials files under the creddir
# If $key is just the host name, then $credfile=$creddir/$host
# If $key is in the form of host/share,
# first look for a file $creddir/$host.$share
# and it does not exist, look for $creddir/$host
export CREDDIR="$HOME/auto.smb.credentials"

# IC patch
# Providing a common CREDFILE for everything, will override the above lookup method
export CREDFILE=

# IC patch
# The problem is that the key ($1) is the full path with the host
# So there should be one key for each directory
# To solve the problem:
# Easy: Get the first part of the $1 (/host/share/...)
# Comprehensive: Look the first two (host and share)

# MN patch: adding port to hostname part
#    use it as: <hostname>:<port>/<share>

# If key does not exist, look just for the host
HOST=${KEY%%/*}
PORT=139

if [[ "$HOST" =~ ':' ]]; 
then 
    PORT=$(echo "$HOST" | cut -f 2 -d :)
    HOST=$(echo "$HOST" | cut -f 1 -d :)
fi
smbopts+=("--port=$PORT")
MOUNTOPTS+=",port=$PORT"

$logger -t autofs.smbng "host: $HOST port: $PORT"

if [ -z "$CREDFILE" ]; then # Search for credentials file in a path under $CREDDIR
    # Look for the file in this order. When several exists the last one to
    # exist wins.
    #   /etc/$host
    #   /etc/$host:$PORT
    #   $creddir/$host:$PORT
    #   $creddir/$host
    credfiles=(
        "/etc/auto.smb.credentials/$HOST"
        "/etc/auto.smb.credentials/$HOST:$PORT"
        "${CREDDIR}/$HOST"
        "${CREDDIR}/$HOST:$PORT"
    )
    for file in ${credfiles[*]};
    do
        if [ -r "$file" ];
        then
            BROWSE_AUTH="$file"
            #$logger "Found credfile: $CREDDIR/$file"
        else
            #$logger "Credfile $creddir/$file not found."
            :
        fi
    done
else
    BROWSE_AUTH=$CREDFILE
fi
$logger "Using credfile: $BROWSE_AUTH"

# find out where smbclient binary is
for path in /bin /sbin /usr/bin /usr/sbin
do
    if [ -x $path/smbclient ]
    then
        SMBCLIENT=$path/smbclient
        break
    fi
done
[ -z "$SMBCLIENT" ] && { $logger 'smbclient binary not found'; exit 1;}

if [[ -n "$BROWSE_AUTH" &&  -f "$BROWSE_AUTH" ]]; 
then
    smbopts+=("-A $BROWSE_AUTH")
else
    smbopts+=("-N")
fi
$logger "smbopts: \"${smbopts[*]}\""

# smbclient output:
#Domain=[LE-SAMBA] OS=[Unix] Server=[Samba 4.1.6-Ubuntu]
#Disk|home-pirate|
#Disk|zalohy|
#IPC|IPC$|IPC Service (This is samba on vostrak)

# Find out all the shares
SHARES=()
while read line; 
do 
    [[ "$line" =~ ^Disk ]] && SHARES+=($(echo $line | cut -f 2 -d \|))
done < <($SMBCLIENT ${smbopts[*]} -gL $HOST 2>/dev/null)
$logger "shares found on $HOST: ${SHARES[*]}"

# http://osr507doc.xinuos.com/en/NetAdminG/autoT.advanced_map_options.html#autoD.hierarch
#
# the '\' in the end of the line is important!
#-fstype=cifs,rw,uid=1000,gid=wheel,port=1391,credfile=/home/me/share.common \
#    "/share1"                                  "://localhost/home-pirate" \
#    "/share2" -credfile=/home/me/share2.cred   "://localhost/owncloud-external"

# common line for all the mounts
ALL_SHARES="-${MOUNTOPTS}"
[ -n "$BROWSE_AUTH" ] && ALL_SHARES+=",credfile=$BROWSE_AUTH"
ALL_SHARES+=' \\\n'

I=0
for SHARE in ${SHARES[*]};
do
    I=$((I+1))

    #look for share specific auth file or use host specific share file or
    #use none, order is impartant, last one to exist wins.
    unset SHARE_AUTH
    CREDFILES=(
        "/etc/auto.smb.credentials/$HOST"
        "/etc/auto.smb.credentials/$HOST:$PORT"
        "/etc/auto.smb.credentials/$HOST.$SHARE"
        "/etc/auto.smb.credentials/$HOST:$PORT.$SHARE"
        "${CREDDIR}/$HOST"
        "${CREDDIR}/$HOST:$PORT"
        "${CREDDIR}/$HOST.$SHARE"
        "${CREDDIR}/$HOST:$PORT.$SHARE"
    )
    for FILE in ${CREDFILES[*]};
    do
        if [ -r "$FILE" ];
        then
            SHARE_AUTH="$FILE"
            $logger "Found credfile: $FILE"
        else
            $logger "Credfile $FILE not found."
        fi
    done
    THIS_SHARE="\"/$SHARE\" "
    #[ -n "$SHARE_AUTH" ] && THIS_SHARE+="-${MOUNTOPTS},credfile=$SHARE_AUTH"
    [ -n "$SHARE_AUTH" ] && THIS_SHARE+="-credfile=$SHARE_AUTH"
    THIS_SHARE+=" \"://$HOST/$SHARE\""
    ALL_SHARES+='\t'
    ALL_SHARES+="$THIS_SHARE"
    [ $I -lt ${#SHARES[*]} ] && ALL_SHARES+=' \\\n'
done
#$logger "$ALL_SHARES"
echo -e ${ALL_SHARES}
