#!/bin/bash
[ -n "$DEBUG" ] && set -x
MY_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
trap 'echo run with DEBUG=1 to see what happens; exit 1' ERR

if [ $(whoami) != 'root' ];
then
    echo 'You need to run the script as root.'
    exit 1
fi

export REMOTE_HOST=${REMOTE_HOST:-'orange'}
export REMOTE_STUNNEL_PORT=${REMOTE_STUNNEL_PORT:-800}

export REMOTE_SHARE=${REMOTE_SHARE:-'home-pirate'}
export REMOTE_SHARE_USER=${REMOTE_SAMBA_USER:-'samba'}
export REMOTE_SHARE_PASSWORD=${REMOTE_SAMBA_PASSWORD:-'password'}

export SAMBA_LOCAL_PORT=${SAMBA_LOCAL_PORT:-1392}
export SAMBA_CRED_DIR=${SAMBA_CRED_DIR:-'/etc/auto.smb.credentials'}

export MNT_REMOTE=${MNT_REMOTE:-'/mnt/remote'}

PACKAGES=(stunnel xinetd smbclient)
apt-get -y install ${PACKAGES[*]}

# create autofs entries
echo "$MNT_REMOTE/ftp /etc/auto.mnt-remote-ftp --timeout=30" > \
    /etc/auto.master.d/remote-ftp.autofs
echo "$MNT_REMOTE/samba /etc/auto.smb" > \
    /etc/auto.master.d/remote-samba.autofs
service autofs restart

# create autofs structure in MNT_REMOTE
mkdir -p ${MNT_REMOTE}/samba
echo 'If you see this you need to start autofs' > ${MNT_REMOTE}/samba/autofs-managed-directory
mkdir -p $HOME/net/${REMOTE_HOST}
ln -fs \
    ${MNT_REMOTE}/samba/localhost:${SAMBA_LOCAL_PORT}/${REMOTE_SHARE} \
    $HOME/net/${REMOTE_HOST}/${REMOTE_SHARE}

# create credentials file
mkdir -p /etc/auto.smb.credentials
chmod 700 /etc/auto.smb.credentials
echo -e "username=$REMOTE_SHARE_USER\npassword=$REMOTE_SHARE_PASSWORD" > \
    ${SAMBA_CRED_DIR}/localhost:${SAMBA_LOCAL_PORT}

# copy scripts/auto.smb to /etc/auto.smb
[ -f ${MY_DIR}/auto.smb ] && ln -fs ${MY_DIR}/auto.smb /etc/auto.smb

# stunnel entry
cat << AA > /etc/stunnel/client-${REMOTE_HOST}-samba.conf
# debug = 7
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1
ciphers = HIGH

client = yes
connect = ${REMOTE_HOST}:${REMOTE_STUNNEL_PORT}
AA
[ -x /etc/init.d/stunnel4 ] && /etc/init.d/stunnel4 stop

# xinetd entry
cat << AA > /etc/xinetd.d/stunnel-client-${REMOTE_HOST}-samba
service stunnel
{
    disable         = no
    port            = ${SAMBA_LOCAL_PORT}
    type            = UNLISTED
    flags           = REUSE
    socket_type     = stream
    wait            = no
    user            = root
    server          = /usr/bin/stunnel
    server_args     = /etc/stunnel/client-${REMOTE_HOST}-samba.conf
}
AA
restart xinetd

