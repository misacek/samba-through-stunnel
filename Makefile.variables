export SUBDIRS := doc

#
# These variables only helps us to create one target in multiple directories
# without recursive call of make. This will also take advantage of paralell make
# if wanted.
#
# The sets of directories to do various things in:
# each will respectively run build, install, test. etc for all $(SUBDIR) directiories
export BUILD_SUBDIRS = $(SUBDIRS:%=build-%)
export INSTALL_SUBDIRS = $(SUBDIRS:%=install-%)
export CLEAN_SUBDIRS = $(SUBDIRS:%=clean-%)
export TEST_SUBDIRS = $(SUBDIRS:%=test-%)

NUMBER_OF_CORES := 1
ifneq ("$(wildcard /proc/cpuinfo)", "")
	NUMBER_OF_CORES := $(shell grep 'core id' /proc/cpuinfo | wc -l)
endif
export JOBSERVER_SUPPORTED :=
export SUBMAKES_PARAMS :=
# Switch on parallelization if supported (https://stackoverflow.com/a/8496333)
# There is this problem where sub-makes will not run more than one job
# unless 'jobserver' feature is introduced
# Check if job server supported:
ifeq ($(filter jobserver, $(.FEATURES)),)
JOBSERVER_SUPPORTED := 0
SUBMAKES_PARAMS := -j$(NUMBER_OF_CORES)
# Job server not supported: sub-makes will only start one job unless
# you specify a higher number here.  Here we use a MS Windows environment
# variable specifying number of processors.
else
# Job server is supported; let GNU Make work as normal (no need to pass -jX
# to submakes: $(MAKE) -jX ...)
JOBSERVER_SUPPORTED := 1
endif

# .FEATURES only works in GNU Make 3.81+.
# If GNU make is older, assume job server support.
ifneq ($(firstword $(sort 3.81 $(MAKE_VERSION))),3.81)
# If you are using GNU Make < 3.81 that does not support job servers, you
# might want to specify -jN parameter here instead.
JOBSERVER_SUPPORTED := 0
SUBMAKES_PARAMS := -j$(NUMBER_OF_CORES)
endif

show-vars: show_vars
show_vars:
	@echo
	@echo BASE Makefile variables:
	@echo BUILD_SUBDIRS:  $(BUILD_SUBDIRS)
	@echo CLEAN_SUBDIRS: $(CLEAN_SUBDIRS)
	@echo INSTALL_SUBDIRS: $(CLEAN_SUBDIRS)
	@echo TEST_SUBDIRS: $(CLEAN_SUBDIRS)
	@echo
	@echo HAVE_SPHINX_APIDOC: $(HAVE_SPHINX_APIDOC)
	@echo HAVE_SPHINX_BUILD: $(HAVE_SPHINX_BUILD)
	@echo
	@echo JOBSERVER_SUPPORTED: $(JOBSERVER_SUPPORTED)
	@echo NUMBER_OF_CORES: $(NUMBER_OF_CORES)
	@echo SUBMAKES_PARAMS: $(SUBMAKES_PARAMS)
	@echo
.PHONY: show_vars
